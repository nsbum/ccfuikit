###About CCFUIKit###

CCFUIKit is a collection of extensions to UIKit that make our lives a little easier when building iOS apps here at Cocoa Factory.

####At a glance####

If you are using CCFUIKit as regular static library, be aware that there are two different versions - a standard version and a legacy version.  The legacy version contains several classes that are used in older projects but that we intend to deprecate over time.

The preferred way of working with CCFUIKit is to use [CocoaPods]() which is a great dependency-management system for Xcode projects.  More about this method of usage below in the Usage section.

From Jiva Devoe's blocks-based UIAlertView and UIActionSheet:

- RIButtonItem
- UIActionSheet+Blocks
- UIAlertView+Blocks

A great blocks-based way of dealing with callbacks from these views.

---

Handy UIColor categories:

- UIColor+CCFExtensions

The method `+colorWithCommaDelimitedDecimalRGBA:` is cool.  Just give it a string like @"123,64,35,1.0" and it will return a UIColor.  Just as cool as this is `+colorWithHexRGB:` where you just give it a string like @"FFB733" and it will return a color.  There are many others.

- UIColor+Components

Mostly, this category provides handy means of accessing individual color components (RGBA)

---

A collection of UIImage categories

There are a number of individual manipulations that you can do on `UIImage` with these categories.  The category names are pretty self-explanatory.

---

Gradient views

We use `CCFLightGradientView` all the time for light backgrounds.  But `CCFGradientView` which is based heavily on code from the Omni Frameworks lets you fully specify a custom two-color gradient view.

---

Web views

Web views are notoriously slow to load even local content.  So we have a couple ways of dealing with that.  `CCFFadingWebView` fades in the content when it's available.  `CCFHUDLoadingWebView` puts up a HUD panel while the content is loading.

Finally, there are tons of other classes here to check out.

####Usage####

Here's an example section of a Podfile that you can use for this:
    pod 'CCFUIKit/main', :git => 'https://nsbum@bitbucket.org/nsbum/ccfuikit.git', 
    :tag => '2.0.4'
    
Note that this just pulls in the main version.  If you select the 'legacy' subspec, you'll get all of the code (main + legacy.)
