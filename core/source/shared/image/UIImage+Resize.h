/**
 *   @file UIImage+Resize.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2009-08-05 09:28:53
 *   @version 1.0
 *
 *   @note Copyright 2009 Trevor Harmon
 *   @note Free for personal or commercial use, with or without modification.  No warranty is expressed or implied.
 */

#import <UIKit/UIImage.h>
#import <UIKit/UIView.h>

@interface UIImage (Resize)
- (UIImage *)croppedImage:(CGRect)bounds;
- (UIImage *)thumbnailImage:(NSInteger)thumbnailSize
          transparentBorder:(NSUInteger)borderSize
               cornerRadius:(NSUInteger)cornerRadius
       interpolationQuality:(CGInterpolationQuality)quality;
- (UIImage *)resizedImage:(CGSize)newSize
     interpolationQuality:(CGInterpolationQuality)quality;
- (UIImage *)resizedImageWithContentMode:(UIViewContentMode)contentMode
                                  bounds:(CGSize)bounds
                    interpolationQuality:(CGInterpolationQuality)quality;
@end
