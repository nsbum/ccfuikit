/**
 *   @file UIImage+LoadPNG.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2010-07-21 09:27:30
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "UIImage+LoadPNG.h"


@implementation UIImage(LoadPNG)

+ (UIImage *)imageFromBundlePNGNamed:(NSString *)nameWithoutExtension;
{
	if( !nameWithoutExtension ) return nil;
	NSString *fpath = [[NSBundle mainBundle] pathForResource:nameWithoutExtension ofType:@"png"];
	if( !fpath ) return nil;
	
	UIImage *img = [UIImage imageWithContentsOfFile:fpath];
	return img;
}

@end
