//
//  UIImage+Mirror.m
//  CCFUIKit
//
//  Created by alanduncan on 1/24/11.
//  Copyright 2011 Cocoa Factory, LLC. All rights reserved.
//

#import "UIImage+Mirror.h"
#import <QuartzCore/QuartzCore.h>
#import <UIKit/UIGraphics.h>
#import <UIKit/UIImageView.h>

@implementation UIImage(Mirror)

- (UIImage *)imageByFlippingVertically;
{
	UIImageView *tempImageView = [[UIImageView alloc] initWithImage:self];
	
	UIGraphicsBeginImageContext(tempImageView.frame.size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGAffineTransform flipVertical = CGAffineTransformMake(1, 0, 0, -1, 0, tempImageView.frame.size.height);
	CGContextConcatCTM(context, flipVertical);  
	
	[tempImageView.layer renderInContext:context];
	
	UIImage *flipedImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return flipedImage;
}


- (UIImage *)imageByFlippingHorizontally;
{
	UIImageView *tempImageView = [[UIImageView alloc] initWithImage:self];
	
	UIGraphicsBeginImageContext(tempImageView.frame.size);
	CGContextRef context = UIGraphicsGetCurrentContext();
	CGAffineTransform xform = CGAffineTransformMakeScale(-1.0, 1.0);
	CGContextConcatCTM(context, xform);  
	
	[tempImageView.layer renderInContext:context];
	
	UIImage *flipedImage = UIGraphicsGetImageFromCurrentImageContext();
	UIGraphicsEndImageContext();
	
	return flipedImage;
}

@end
