/**
 *   @file UIImage+Alpha.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2009-09-20 09:25:54
 *   @version 1.0
 *
 *   @note Copyright 2009 Trevor Harmon
 */
#import <UIKit/UIImage.h>

#ifndef CCF_UIIMAGE_ALPHA_H
#define CCF_UIIMAGE_ALPHA_H

@interface UIImage(Alpha)
- (BOOL)hasAlpha;
- (UIImage *)imageWithAlpha;
- (UIImage *)transparentBorderImage:(NSUInteger)borderSize;
@end

#endif