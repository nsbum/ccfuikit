/**
 *   @file UIImage+RoundedCorner.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2009-09-20 09:32:05
 *   @version 1.0
 *
 *   @note Copyright 2009 Trevor Harmon
 *   @note Free for personal or commercial use, with or without modification.  No warranty is expressed or implied.
 *   @note Extends the UIImage class to support making rounded corners
 */

#import <UIKit/UIImage.h>

@interface UIImage (RoundedCorner)
- (UIImage *)roundedCornerImage:(NSInteger)cornerSize borderSize:(NSInteger)borderSize;
@end
