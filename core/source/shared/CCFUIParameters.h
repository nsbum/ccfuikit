/**
 *   @file CCFUIParameters.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2011-07-15 08:17:13
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#ifndef CCFUIKit_CCFUIParameters_h
#define CCFUIKit_CCFUIParameters_h

// CCFUIGradientView
#define kCCFUIShadowEdgeThickness (6.0f)
#define kCCFUIShadowEdgeMaximumAlpha (0.4f)

#define kCCFUIToolbarEdgePadding (5.0f)
#define kCCFUIToolbarIteritemPadding (12)

#endif
