#import "CCFUIParameters.h"

//  Block-based alerts and action sheets
#import "RIButtonItem.h"
#import "UIActionSheet+Blocks.h"
#import "UIAlertView+Blocks.h"

#import "RGBtoHSB.h"
#import "RRCausticColorMatcher.h"
#import "RRExponentialFunction.h"
#import "RRGlossCausticShader+SaveToDictionary.h"
#import "RRGlossCausticShader.h"
#import "RRLuminanceFromRGBComponents.h"
#import "UIButton+Glossy.h"
#import "UIButton+Blocks.h"

#pragma mark - Color
#import "UIColor+CCFExtensions.h"
#import "UIColor+Components.h"

#pragma mark - Image
#import "UIImage+Alpha.h"
#import "UIImage+LoadPNG.h"
#import "UIImage+Mirror.h"
#import "UIImage+Resize.h"
#import "UIImage+RotationExtension.h"
#import "UIImage+RoundedCorner.h"
#import "UIImage+ScalingExtension.h"


#pragma mark - View
#import "UIView+CCFExtensions.h"
#pragma mark Gradient views
#import "APGBackgroundGradientView.h"
#import "APGFullBackgroundGradientView.h"
#import "CCFBottomStatusPanelView.h"
#import "CCFGradientView.h"
#import "CCFLightGradientView.h"
#import "CCFMetallicToolbarView.h"
#import "CCFStatusPanelView.h"

#pragma mark Label
#import "CCFVerticallyAlignedLabel.h"
#import "RRSGlowLabel.h"
#import "UILabel+CCFExtensions.h"

#pragma mark TextView
#import "SSTextView.h"

#pragma mark Web
#import "CCFFadingWebView.h"
#import "CCFHUDLoadingWebView.h"

#pragma mark - Legacy support
#ifdef CCFUIKIT_LEGACY
#pragma mark AKCascadingStyle
#import "AKCascadingStyle.h"
#import "CCFCascadingStyle.h"
#import "NSObject+AKCascadingStyle.h"
#import "UIColor+AKCascadingStyle.h"
#import "UIFont+AKCascadingStyle.h"

#pragma mark Button
#import "button-med-back-highlight.png.h"
#import "button-med-back.png.h"
#import "button-next-highlight.png.h"
#import "button-next.png.h"
#import "ccf_cancel_button_highlight_png.h"
#import "ccf_cancel_button_png.h"
#import "ccf_ok_button_highlight_png.h"
#import "ccf_ok_button_png.h"
#import "CCFCancelButton.h"
#import "CCFImageBasedGlossyButton.h"
#import "CCFMediumBackButton.h"
#import "CCFMediumNextButton.h"
#import "CCFOKButton.h"

#pragma mark Controller
#import "CCFAppController.h"
#import "CCFImageViewController.h"
#import "CCFSingleChoiceTableViewController.h"
#import "CCFTableViewController.h"
#import "CCFViewController.h"

#pragma mark FontLabel
#import "FontLabel.h"
#import "FontLabelStringDrawing.h"
#import "FontManager.h"
#import "ZAttributedString.h"
#import "ZFont.h"

#pragma mark Table
#import "CCFClearLabelsCellView.h"
#import "CCFGroupedCell.h"
#import "CCFGroupedCellBackgroundView.h"
#import "CCFGroupedTableViewController.h"
#import "CCFShadowedTableView.h"
#import "CCFTableCellGradientView.h"
#import "PageCell.h"
#import "PageCellBackground.h"
#import "PageViewController.h"
#import "RoundRect.h"
#import "TDBadgedCell.h"
#import "UACellBackgroundView.h"

#endif
