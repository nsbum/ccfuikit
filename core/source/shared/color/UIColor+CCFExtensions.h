/**
 *   @file UIColor+CCFExtensions.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2010-06-02 09:39:39
 *   @version 1.0
 *
 *   @note  Copyright (c) 2011 Cocoa Factory, LLC. All rights reserved.
 *          Permission is hereby granted, free of charge, to any person obtaining a copy of this
 *          software and associated documentation files (the "Software"), to deal in the
 *          Software without restriction, including without limitation the rights to use, copy,
 *          modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 *          and to permit persons to whom the Software is furnished to do so, subject to the
 *          following conditions:
 *          The above copyright notice and this permission notice shall be included in all
 *          copies or substantial portions of the Software.
 *          THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *          INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 *          PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *          HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 *          CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 *          OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#import <UIKit/UIColor.h>

/*!
	@category UIColor(CCFExtensions)
	@abstract Cocoa Factory extensions to UIColor
 */
@interface UIColor(CCFExtensions)

@property (nonatomic, readonly) CGColorSpaceModel colorSpaceModel;
@property (nonatomic, readonly) BOOL canProvideRGBComponents;
@property (nonatomic, readonly) CGFloat red; // Only valid if canProvideRGBComponents is YES
@property (nonatomic, readonly) CGFloat green; // Only valid if canProvideRGBComponents is YES
@property (nonatomic, readonly) CGFloat blue; // Only valid if canProvideRGBComponents is YES
@property (nonatomic, readonly) CGFloat white; // Only valid if colorSpaceModel == kCGColorSpaceModelMonochrome
@property (nonatomic, readonly) CGFloat alpha;

/*!
    @property   rgbHex
    @brief      UInt32 representation of color when formatted as hex
    @details    Omits alpha component
*/
@property (nonatomic, readonly) UInt32 rgbHex;
@property (nonatomic, readonly) CGFloat hue; // Only valid if canProvideRGBComponents is YES
@property (nonatomic, readonly) CGFloat saturation; // Only valid if canProvideRGBComponents is YES
@property (nonatomic, readonly) CGFloat brightness; // Only valid if canProvideRGBComponents is YES
@property (nonatomic, readonly) CGFloat value; // (same as brightness, added for naming consistency)

/*!
    @method     red:green:blue:alpha:
    @brief      Obtains components from an RGBA color
    @param      r Pointer to red value
    @param      g Pointer to green value
    @param      b Pointer to blue value
    @param      a Pointer to alpha value
    @return     All RGBA values by reference
*/
- (BOOL)red:(CGFloat *)r green:(CGFloat *)g blue:(CGFloat *)b alpha:(CGFloat *)a;


/*!
    @method     colorWithCommaDelimitedDecimalRGBA:
    @brief      Returns RGB color from a comma delimited list of numbers
    @details    The first three numbers are 256 based, the alpha (last) component is 0.0 - 1.0 based float
    @param      colorString The string containing the numbers
    @return     The color
*/
+ (UIColor *)colorWithCommaDelimitedDecimalRGBA:(NSString *)colorString;


/*!
    @method     colorWithHexRGB:
    @brief      Returns RGB color from a hex string
    @details    Use hex string format like CSS.  As an example: \code UIColor *someColor = [UIColor colorWithHexRGB:@"#ff032a"]; \endcode
    @param      hexString Formatted like @"#fb023a"
    @return     The color
*/
+ (UIColor *)colorWithHexRGB:(NSString *)hexString;


/*!
    @method     colorWithRGBHex:
    @brief      Returns RGB color from a UInt32 representation of hex
    @details    Must provide the integer amount of the hex digit representation.  
 As an example \code UIColor *someColor = [UIColor colorWithRGBHex:16712490]; \endcode  This example gives the same color as the hex example above.
    @param      hex The color as long integer
    @return     The color
*/
+ (UIColor *)colorWithRGBHex:(UInt32)hex;

+ (UIColor *)randomColor;

/*!
    @method     colorByName:
    @brief      Returns a color by its CSS name
    @details    The CSS color names can be found at http://www.w3schools.com/css/css_colornames.asp
    @param      cssColorName CSS color name as string
    @return     The color
*/
+ (UIColor *)colorByName:(NSString *)cssColorName;


/** @name    RGB manipulation
			 Altering RGB components */
//@{

- (BOOL)canProvideRGBComponents;
- (UIColor *)colorByLuminanceMapping;

- (UIColor *)colorByMultiplyingByRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha;
- (UIColor *)colorByAddingRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha;
- (UIColor *)colorByLighteningToRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha;
- (UIColor *)colorByDarkeningToRed:(CGFloat)red green:(CGFloat)green blue:(CGFloat)blue alpha:(CGFloat)alpha;

- (UIColor *)colorByMultiplyingBy:(CGFloat)f;
- (UIColor *)colorByAdding:(CGFloat)f;
- (UIColor *)colorByLighteningTo:(CGFloat)f;
- (UIColor *)colorByDarkeningTo:(CGFloat)f;

- (UIColor *)colorByMultiplyingByColor:(UIColor *)color;
- (UIColor *)colorByAddingColor:(UIColor *)color;
- (UIColor *)colorByLighteningToColor:(UIColor *)color;
- (UIColor *)colorByDarkeningToColor:(UIColor *)color;

//@}


/** @name    HSV
			 HSV/HSL component access and manipulation */
//@{

- (UIColor *)colorByIncreasingValueByFactor:(CGFloat)factor;

- (UIColor *)colorByReducingValueByFactor:(CGFloat)factor;

//	#import "UIColor-Expanded.h"
//  get it here:
//  http://github.com/ars/uicolor-utilities

#define MIN3(x,y,z)  ((y) <= (z) ? \
((x) <= (y) ? (x) : (y)) \
: \
((x) <= (z) ? (x) : (z)))

#define MAX3(x,y,z)  ((y) >= (z) ? \
((x) >= (y) ? (x) : (y)) \
: \
((x) >= (z) ? (x) : (z)))


struct rgb_color {
    CGFloat r, g, b;
};

struct hsv_color {
    CGFloat hue;        
    CGFloat sat;        
    CGFloat val;        
};

+ (struct hsv_color)HSVfromRGB:(struct rgb_color)rgb;
-(CGFloat)hue;
-(CGFloat)saturation;
-(CGFloat)brightness;
-(CGFloat)value;

//@}

/*!
	\mainpage	UIColor+CCFExtensions
	\section intro_sec Introduction
 
 Documentation for Cocoa Factory, LLC extensions to UIColor.  These extensions provide access to color components in both RGB and HSV color models.  It also provides a number of conveniences for creating colors from hex strings, long integers, etc.
 */

@end
