//
//  RIButtonItem.h
//  Shibui
//
//  Created by Jiva DeVoe on 1/12/11.
//  Copyright 2011 Random Ideas, LLC. All rights reserved.
//

#ifndef _RI_BUTTON_ITEM_H
#define _RI_BUTTON_ITEM_H
typedef void (^RISimpleAction)();

@interface RIButtonItem : NSObject
{
    NSString *label;
    RISimpleAction action;
}
@property ( nonatomic, strong)  NSString *label;
@property (copy, nonatomic) RISimpleAction action;
+(id)item;
+(id)itemWithLabel:(NSString *)inLabel;
@end

#endif