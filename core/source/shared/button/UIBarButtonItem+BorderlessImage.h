/**
 *   @file UIBarButtonItem+BorderlessImage.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2013-06-03 05:05:25
 *
 *   @note Copyright 2013 Cocoa Factory, LLC.  All rights reserved
 */


@interface UIBarButtonItem (BorderlessImage)

+ (UIBarButtonItem *)barItemWithImage:(UIImage *)image target:(id)target action:(SEL)selector;

@end
