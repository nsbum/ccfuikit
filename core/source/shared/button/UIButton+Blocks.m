/**
 *   @file UIButton(Blocks).m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2011-06-10 05:04:23
 *   @version 1.0
 *
 *   @note  Copyright (c) 2011 Cocoa Factory, LLC. All rights reserved.
 *          Permission is hereby granted, free of charge, to any person obtaining a copy of this
 *          software and associated documentation files (the "Software"), to deal in the
 *          Software without restriction, including without limitation the rights to use, copy,
 *          modify, merge, publish, distribute, sublicense, and/or sell copies of the Software,
 *          and to permit persons to whom the Software is furnished to do so, subject to the
 *          following conditions:
 *          The above copyright notice and this permission notice shall be included in all
 *          copies or substantial portions of the Software.
 *          THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR IMPLIED,
 *          INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY, FITNESS FOR A 
 *          PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE AUTHORS OR COPYRIGHT
 *          HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER LIABILITY, WHETHER IN AN ACTION OF
 *          CONTRACT, TORT OR OTHERWISE, ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE 
 *          OR THE USE OR OTHER DEALINGS IN THE SOFTWARE.
 *
 */

#import "UIButton+Blocks.h"
#import <objc/runtime.h>

static NSString *CCF_BUTTON_ITEM_ASSOCIATION_KEY = @"com.cocoafactory.buttonitem";

@implementation UIButton (UIButton_Blocks)

- (id)initWithDarkInfoButtonBlock:(void (^)(NSInteger section))aBlock;
{
    self = [UIButton buttonWithType:UIButtonTypeInfoDark];
    if( !self ) return nil;
    
    objc_setAssociatedObject(self,(__bridge const void *)CCF_BUTTON_ITEM_ASSOCIATION_KEY,aBlock,OBJC_ASSOCIATION_COPY_NONATOMIC);
    [self addTarget:objc_getAssociatedObject(self, (__bridge const void *)CCF_BUTTON_ITEM_ASSOCIATION_KEY)
             action:@selector(CCFTableViewSectionHeaderButtonCallbackBlock:)
   forControlEvents:UIControlEventTouchUpInside];
    
    return self;
}

+ (UIButton *)darkInfoButtonWithBlock:(void (^)(NSInteger section))aBlock;
{
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeInfoDark];
    if( !self ) return nil;
    
    objc_setAssociatedObject(self,(__bridge const void *)CCF_BUTTON_ITEM_ASSOCIATION_KEY,aBlock,OBJC_ASSOCIATION_COPY_NONATOMIC);
    [button addTarget:objc_getAssociatedObject(self, (__bridge const void *)CCF_BUTTON_ITEM_ASSOCIATION_KEY)
             action:@selector(CCFTableViewSectionHeaderButtonCallbackBlock:)
   forControlEvents:UIControlEventTouchUpInside];
    
    return button;
}

@end
