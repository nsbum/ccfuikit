#import "UIBarButtonItem+BorderlessImage.h"
#import <objc/runtime.h>

@implementation UIBarButtonItem (BorderlessImage)

+ (UIBarButtonItem *)barItemWithImage:(UIImage *)image target:(id)target action:(SEL)selector {
    NSAssert(image, @"image is missing");
    NSAssert(target, @"target is missing");
    
    UIButton *button = [[UIButton alloc] initWithFrame:CGRectMake(0, 0, image.size.width, image.size.height)];
    [button setImage:image forState:UIControlStateNormal];
    [button addTarget:target action:selector forControlEvents:UIControlEventTouchUpInside];
    
    UIBarButtonItem *barItem = [[UIBarButtonItem alloc] initWithCustomView:button];
    
    objc_setAssociatedObject(button, "barButtonItemKey", barItem, OBJC_ASSOCIATION_ASSIGN);
    
    return barItem;
}

@end
