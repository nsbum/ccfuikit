/**
 *   @file CCFBarButtonItem.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2011-04-30 05:58:11
 *
 *   @note Copyright 2013 Cocoa Factory, LLC.  All rights reserved
 */

@interface NSObject (CCFBlockCallback)
- (void)CCFCallbackBlock;
- (void)CCFCallbackBlockWithSender:(id)sender;
@end

@implementation NSObject(CCFBlockCallback)
- (void)CCFCallbackBlock
{
    void (^block)(void) = (id)self;
    block();
}
- (void)CCFCallbackBlockWithSender:(id)sender {
    void (^block)(id obj) = (id)self;
    block(sender);
}
@end

@interface CCFBarButtonItem : UIBarButtonItem
{
@protected
    id _originalTarget;
}

- (id)initWithImage:(UIImage *)image target:(id)target action:(SEL)action;
- (id)initWithBarButtonSystemItem:(UIBarButtonSystemItem)systemItem block:(void (^)(id sender))block;
- (id)initWithImage:(UIImage *)image style:(UIBarButtonItemStyle)style block:(void (^)(id sender))aBlock;
- (id)initWithTitle:(NSString *)title style:(UIBarButtonItemStyle)style block:(void (^)(id sender))aBlock;
- (id)initWithLightInfoButtonBlock:(void (^)(id sender))aBlock;
- (id)initWithLightInfoButtonTarget:(id)target action:(SEL)action;

@end
