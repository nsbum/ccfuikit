/*
 
 File: CCFBarButtonItem.m
 
 Abstract: Block-based bar button item
 
 License:  IMPORTANT: This Cocoa Factory, LLC software is supplied to you in consideration of your
 agreement to the following terms, and your use or installation of this Cocoa Factory software constitutes
 acceptance of these terms.  If you do not agree with these terms, you may not use or install this software.
 
 In consideration of your agreement to abide by the following terms, and subject to these terms,
 Cocoa Factory grants you a personal, non-exclusive license, under Cocoa Factory's copyrights in this original
 Cocoa Factory software (the "CCF Software") to use without modification in source and/or binary form.  Your
 use of the CCF Software is restricted to its role in building the application specified by the project or
 workspace (the "Application") in which it is contained.  You may not extract the CCF Software from the Application
 for any purpose.  You may not duplicate the CCF Software or transmit it in any form outside the Application.
 
 Except as expressly stated in this notice, no other rights or licenses, express or implied, are granted by
 Cocoa Factory herein, including but not limited to any patent rights that may be infringed by derivative works.
 
 The CCF Software is provided by Cocoa Factory on an "AS IS" basis.  COCOA FACTORY MAKES NO WARRANTIES, EXPRESS
 OR IMPLIED, INCLUDING WITHOUT LIMITATION THE IMPLIED WARRANTIES OF NON-INFRINGEMENT, MERCHANTABILITY AND FITNESS
 FOR A PARTICULAR PURPOSE REGARDING THE CCF SOFTWARE OR ITS USE AND OPERATION IN THE APPLICATION IN WHICH IT IS 
 DISTRIBUTED.
 
 IN NO EVENT SHALL COCOA FACTORY, LLC BE LIABLE FOR ANY SPECIAL, INDIRECT, INCIDENTAL, OR CONSEQUENTIAL DAMAGES
 (INCLUDING, BUT NOT LIMITED TO, PROCUREMEN OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS 
 INTERRUPTION) ARISING IN ANY WAY OUT OF THE USE, REPRODUCTION, MODIFICATION AND/OR DISTRIBUTION OF THE COCOA FACTORY
 SOFTWARE, HOWEVER CAUSED AND WHETHER UNDER THEORY OF CONTRACT, TORT (INCLUDING NEGLIGENCE), STRICT LIABILITY OR OTHERWISE,
 EVEN IF COCOA FACTORY HAS BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 
 Copyright (c) 2011 Cocoa Factory, LLC All Rights Reserved
 
 */

#import "CCFBarButtonItem.h"
#import <objc/runtime.h>

static NSString * const CCF_BAR_BUTTON_ITEM_ASSOCIATION_KEY = @"com.cocoafactory.barbuttonitem";

@implementation CCFBarButtonItem

#pragma mark - Object life cycle

- (id)initWithImage:(UIImage *)image target:(id)target action:(SEL)action;
{
    _originalTarget = target;
    
    UIButton *imgButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [imgButton setImage:image forState:UIControlStateNormal];
    imgButton.frame = CGRectMake(0.0, 0.0, image.size.width, image.size.height);
    [imgButton addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    
    self = [super initWithCustomView:imgButton];
    
    return self;
}

- (id)initWithLightInfoButtonBlock:(void (^)(id sender))aBlock;
{
    UIButton *tempButton = [UIButton buttonWithType:UIButtonTypeInfoLight];
    UIImage *image = [tempButton currentImage];
    
    return [self initWithImage:image style:UIBarButtonItemStylePlain block:aBlock];
}

- (id)initWithLightInfoButtonTarget:(id)target action:(SEL)action;
{
    _originalTarget = target;
    
    UIButton *button = [UIButton buttonWithType:UIButtonTypeInfoLight];
    [button setFrame:CGRectMake(0, 0, 25, 25)];
    [button addTarget:self action:action forControlEvents:UIControlEventTouchUpInside];
    
    return [super initWithCustomView:button];
}

- (NSMethodSignature *)methodSignatureForSelector:(SEL)aSelector;
{
    if( [_originalTarget respondsToSelector:aSelector] )
    {
        return [_originalTarget methodSignatureForSelector:aSelector];
    }
    else
    {
        return [super methodSignatureForSelector:aSelector];
    }
}

- (void)forwardInvocation:(NSInvocation *)anInvocation;
{
    SEL aSelector = [anInvocation selector];
    if( [_originalTarget respondsToSelector:aSelector] )
    {
        //  modify the 'sender' argument so that it points to self
        __unsafe_unretained CCFBarButtonItem *weakSelf = self;
        [anInvocation setArgument:&weakSelf atIndex:2];
        [anInvocation invokeWithTarget:_originalTarget];
    }
    else
    {
        [self doesNotRecognizeSelector:aSelector];
    }
}

- (id)initWithBarButtonSystemItem:(UIBarButtonSystemItem)systemItem block:(void (^)(id sender))aBlock;
{
    self = [super initWithBarButtonSystemItem:systemItem target:nil action:@selector(CCFCallbackBlockWithSender:)];
    if( !self ) { return nil; }
    
    objc_setAssociatedObject(self,(__bridge void *)CCF_BAR_BUTTON_ITEM_ASSOCIATION_KEY,aBlock,OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.target = objc_getAssociatedObject(self, (__bridge void *)CCF_BAR_BUTTON_ITEM_ASSOCIATION_KEY);
    
    return self;
}

- (id)initWithImage:(UIImage *)image style:(UIBarButtonItemStyle)style block:(void (^)(id sender))aBlock;
{
    self = [super initWithImage:image style:style target:nil action:@selector(CCFCallbackBlockWithSender:)];
    if( !self ) { return nil; }
    
    objc_setAssociatedObject(self,(__bridge void *)CCF_BAR_BUTTON_ITEM_ASSOCIATION_KEY,aBlock,OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.target = objc_getAssociatedObject(self, (__bridge void *)CCF_BAR_BUTTON_ITEM_ASSOCIATION_KEY);
    
    return self;
}

- (id)initWithTitle:(NSString *)title style:(UIBarButtonItemStyle)style block:(void (^)(id sender))aBlock;
{
    self = [super initWithTitle:title style:style target:nil action:@selector(CCFCallbackBlockWithSender:)];
    if( !self ) { return nil; }
    
    objc_setAssociatedObject(self,(__bridge void *)CCF_BAR_BUTTON_ITEM_ASSOCIATION_KEY,aBlock,OBJC_ASSOCIATION_COPY_NONATOMIC);
    self.target = objc_getAssociatedObject(self, (__bridge void *)CCF_BAR_BUTTON_ITEM_ASSOCIATION_KEY);
    
    return self;
}

@end
