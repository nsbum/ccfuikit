#import "CCFClearBackgroundView.h"

@implementation CCFClearBackgroundView

static CCFClearBackgroundView *commonInit(CCFClearBackgroundView *self)
{
    self.backgroundColor = [UIColor clearColor];
    return self;
}

- (id)initWithFrame:(CGRect)frame
{
    self = [super initWithFrame:frame];
    if( !self ) return nil;
    
    return commonInit(self);
}

- (id)initWithCoder:(NSCoder *)aDecoder {
    self = [super initWithCoder:aDecoder];
    if( !self ) return nil;
    
    return commonInit(self);
}


@end
