/**
 *   @file CCFClearBackgroundView.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2013-07-21 06:28:35
 *
 *   @note Copyright 2013 Cocoa Factory, LLC.  All rights reserved
 */

@interface CCFClearBackgroundView : UIView

@end
