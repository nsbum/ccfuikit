/**
 *   @file CCFFadingWebView.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2010-07-21 09:45:00
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */
#import "CCFFadingWebView.h"


static const NSTimeInterval defaultDuration = 1.2f;
static const BOOL WantsBackgroundView = YES;

// -------------------------------------------------------------------------------
//
//	Private method declarations
//
// -------------------------------------------------------------------------------
@interface CCFFadingWebView ()
- (UIView *)_viewForBackground;
- (CABasicAnimation *)_animationForFadeIn;
@end


@implementation CCFFadingWebView

@synthesize fadeDuration = _fadeDur;
@synthesize bgColor = _bgColor;

- (id)initWithFrame:(CGRect)frame fadeDuration:(NSTimeInterval)dur backgroundColor:(UIColor *)aColor;
{
	if( !aColor ) return nil;
	
	self = [super initWithFrame:frame];
	if( !self ) return nil;
	
	_fadeDur = (dur > 0)?dur:defaultDuration;
    _bgColor = aColor;
	
	[self setHidden:YES];
	
	return self;
}

- (void)loadLocalHTMLFileName:(NSString *)fn;
{
		//NSLog(@"start");
	if( !fn ) return;
	
	if( WantsBackgroundView )
	{
        _bgView = [self _viewForBackground];
		[[self superview] insertSubview:_bgView belowSubview:self];
	}
	
	NSString *htmlPath = [[NSBundle mainBundle] pathForResource:fn ofType:@"html"];
	NSURL *theURL = [NSURL fileURLWithPath:htmlPath];
	NSURLRequest *request = [NSURLRequest requestWithURL:theURL];
		//NSLog(@"request = %@",request);
	[self loadRequest:request];
	
	CABasicAnimation *anim = [self _animationForFadeIn];
	[anim setDelegate:self];
	[[self layer] addAnimation:[self _animationForFadeIn] forKey:@"opacity"];
	[self setHidden:NO];
}

#pragma mark - CAAnimation delegate

- (void)animationDidStop:(CAAnimation *)anim finished:(BOOL)flag;
{
	if( !WantsBackgroundView ) return;
	[_bgView removeFromSuperview];
}

#pragma mark -
#pragma mark Private methods

- (UIView *)_viewForBackground;
{
	if( !_bgColor ) return nil;
	UIView *aView = [[UIView alloc] initWithFrame:[self frame]];
	[aView setBackgroundColor:_bgColor];
	return aView;
}

- (CABasicAnimation *)_animationForFadeIn;
{
	CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"opacity"];
	[anim setFromValue:[NSNumber numberWithFloat:0.0]];
	[anim setToValue:[NSNumber numberWithFloat:1.0]];
	[anim setDuration:_fadeDur];
	[anim setRepeatCount:0.0];
	[anim setFillMode:kCAFillModeForwards];
	[anim setRemovedOnCompletion:NO];
	return anim;
}


@end


