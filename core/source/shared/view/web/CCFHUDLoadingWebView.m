/**
 *   @file CCFHUDLoadingWebView.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2010-10-28 09:48:03
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */


#import "CCFHUDLoadingWebView.h"
#import "MBProgressHUD.h"

@implementation CCFHUDLoadingWebView

- (id)initWithFrame:(CGRect)frame;
{
	self = [super initWithFrame:frame];
	if( !self ) return nil;
	
	[self setDelegate:self];
	return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder;
{
	self = [super initWithCoder:aDecoder];
	if( !self ) return nil;
	
	[self setScalesPageToFit:YES];
	[self setDelegate:self];
	return self;
}

#pragma mark -
#pragma mark Subviews

- (MBProgressHUD *)progressHUD;
{
	if( !_progressHUD )
	{
		MBProgressHUD *hud = [[MBProgressHUD alloc] initWithView:self];
        _progressHUD = hud;
		[_progressHUD setLabelText:@"Loading..."];
	}
	return _progressHUD;
}

#pragma mark -
#pragma mark UIWebViewDelegate

- (void)webViewDidStartLoad:(UIWebView *)theWebView;
{
	if( [theWebView isEqual:self] )
	{	
		[self addSubview:[self progressHUD]];
		[[self progressHUD] show:YES];
		_loadInterval = [NSDate timeIntervalSinceReferenceDate];
	}
}

- (void)webViewDidFinishLoad:(UIWebView *)theWebView;
{
	if( [theWebView isEqual:self] )
	{
		[[self progressHUD] show:NO];
		[[self progressHUD] removeFromSuperview];
	}
}

- (void)webView:(UIWebView *)theWebView didFailLoadWithError:(NSError *)error;
{
	NSLog(@"error code = %d",[error code] );
	[[self progressHUD] show:NO];
	[[self progressHUD] removeFromSuperview];
	
	UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Error loading page" 
													message:@"Unable to load page" 
												   delegate:nil 
										  cancelButtonTitle:nil 
										  otherButtonTitles:@"OK",nil];
	[alert show];
}


@end
