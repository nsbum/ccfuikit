/**
 *   @file RRSGlowLabel.h
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2010-04-28 09:35:10
 *   @version 1.0
 *
 *   @note Copyright 2010 Red Robot Studios. All rights reserved.
 *   @note 2011-07-20 09-36-50 This code is publically available on github; but no licensing terms are noted.  By default
 *         this is public domain.
 */

#import <UIKit/UIKit.h>

#ifndef __RRSGLOWLABEL_H
#define __RRSGLOWLABEL_H

@interface RRSGlowLabel : UILabel
{
	CGSize glowOffset;
    UIColor *glowColor;
    CGFloat glowAmount;
}

@property (nonatomic, assign) CGSize glowOffset;
@property (nonatomic, assign) CGFloat glowAmount;

#if __has_feature(objc_arc)
@property (nonatomic, strong) UIColor *glowColor;
#else
@property (nonatomic, retain) UIColor *glowColor;
#endif

@end

#endif