/**
 *   @file RRSGlowLabel.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2010-04-28 09:35:10
 *   @version 1.0
 *
 *   @note Copyright 2010 Red Robot Studios. All rights reserved.
 *   @note 2011-07-20 09-36-50 This code is publically available on github; but no licensing terms are noted.  By default
 *         this is public domain.
 */

#import "RRSGlowLabel.h"


@implementation RRSGlowLabel


@synthesize glowColor, glowOffset, glowAmount;

- (id)initWithFrame:(CGRect)frame;
{
    self = [super initWithFrame:frame];
    if(self != nil) {
        self.glowOffset = CGSizeMake(0.0, 0.0);
        self.glowAmount = 0.0;
        self.glowColor = [UIColor clearColor];
    }
    return self;
}

- (void)drawTextInRect:(CGRect)rect;
{
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSaveGState(context);
    
    CGContextSetShadow(context, self.glowOffset, self.glowAmount);
    CGColorSpaceRef colorSpace = CGColorSpaceCreateDeviceRGB();
    
    CGColorRef color = CGColorCreate(colorSpace, CGColorGetComponents(self.glowColor.CGColor));
    CGContextSetShadowWithColor(context, self.glowOffset, self.glowAmount, color);
    
    [super drawTextInRect:rect];
    
    CGColorRelease(color);
    CGColorSpaceRelease(colorSpace);
    CGContextRestoreGState(context);
}


@end
