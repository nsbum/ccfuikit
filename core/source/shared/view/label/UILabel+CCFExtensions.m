/**
 *   @file UILabel+CCFExtensions.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2010-12-01 09:38:12
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "UILabel+CCFExtensions.h"
#import "UIColor+CCFExtensions.h"

@implementation UILabel(CCFExtensions)

+ (UILabel *)tableFooterLabelWithFrame:(CGRect)aFrame;
{
	UILabel *footerLabel = [[UILabel alloc] initWithFrame:CGRectMake(20, 5, 280, 125)];
	footerLabel.backgroundColor = [UIColor clearColor];
	footerLabel.font = [UIFont boldSystemFontOfSize:15];
	footerLabel.shadowColor = [UIColor colorWithWhite:1.0 alpha:0.5];
	footerLabel.textColor = [UIColor colorWithCommaDelimitedDecimalRGBA:@"76,96,108,1.0"];
	footerLabel.shadowOffset = CGSizeMake(0, -1);
#if __IPHONE_OS_VERSION_MAX_ALLOWED >= 60000
    footerLabel.textAlignment = NSTextAlignmentCenter;
#else
	footerLabel.textAlignment = UITextAlignmentCenter;
#endif
	return footerLabel;
}

+ (UILabel *)tableHeaderLabelWithFrame:(CGRect)aFrame;
{
    UILabel *headerLabel = [[UILabel alloc] initWithFrame:aFrame];
    headerLabel.backgroundColor = [UIColor clearColor];
    headerLabel.font = [UIFont boldSystemFontOfSize:17.0];
    headerLabel.shadowColor = [UIColor colorWithWhite:1.0 alpha:1.0f];
    headerLabel.shadowOffset = CGSizeMake(0.0f, 1.0f);
    headerLabel.textColor = [UIColor colorWithRed:0.265 green:0.294 blue:0.367 alpha:1.000];

    return headerLabel;
}

- (void)alignTop;
{
    CGSize fontSize = [self.text sizeWithFont:self.font];
    double finalHeight = fontSize.height * self.numberOfLines;
    double finalWidth = self.frame.size.width;    //expected width of label
    CGSize theStringSize = [self.text sizeWithFont:self.font constrainedToSize:CGSizeMake(finalWidth, finalHeight) lineBreakMode:self.lineBreakMode];
    int newLinesToPad = (finalHeight  - theStringSize.height) / fontSize.height;
    for(int i=0; i<newLinesToPad; i++)
        self.text = [self.text stringByAppendingString:@"\n "];
}
	
- (void)alignBottom;
{
    CGSize fontSize = [self.text sizeWithFont:self.font];
    double finalHeight = fontSize.height * self.numberOfLines;
    double finalWidth = self.frame.size.width;    //expected width of label
    CGSize theStringSize = [self.text sizeWithFont:self.font constrainedToSize:CGSizeMake(finalWidth, finalHeight) lineBreakMode:self.lineBreakMode];
    int newLinesToPad = (finalHeight  - theStringSize.height) / fontSize.height;
    for(int i=0; i<newLinesToPad; i++)
        self.text = [NSString stringWithFormat:@" \n%@",self.text];
}

@end
