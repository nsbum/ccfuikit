/**
 *   @file UIView+CCFExtensions.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2010-07-13 09:45:59
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "UIView+CCFExtensions.h"
#import <UIKit/UIKit.h>

@implementation UIView(CCFExtensions)

- (void)moveSubviewWithTagToFront:(NSUInteger)aTag;
{
	UIView *aView = [self viewWithTag:aTag];
	if( aView == nil ) return;
	
	[self bringSubviewToFront:aView];
}

- (void)sendSubviewWithTagToBack:(NSUInteger)aTag;
{
	UIView *aView = [self viewWithTag:aTag];
	if( aView == nil ) return;
	
	[self sendSubviewToBack:aView];
}

@end
