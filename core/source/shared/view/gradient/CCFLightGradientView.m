/**
 *   @file CCFLightGradientView.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2010-09-09 09:14:23
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFLightGradientView.h"
#import <QuartzCore/QuartzCore.h>

@implementation CCFLightGradientView

#define WANTS_LIGHT_GRADIENT_VIEW_SHADOW 0
- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
#if WANTS_LIGHT_GRADIENT_VIEW_SHADOW
        CALayer *baseLayer = [self layer];
		[baseLayer setShadowOpacity:0.4];
		[baseLayer setShadowColor:[[UIColor blackColor] CGColor]];
		[baseLayer setShadowRadius:5.0];
#endif
    }
    return self;
}

- (void)awakeFromNib;
{
	//return;
#if WANTS_LIGHT_GRADIENT_VIEW_SHADOW
	CALayer *baseLayer = [self layer];
	[baseLayer setShadowOpacity:0.6];
	[baseLayer setShadowColor:[[UIColor blackColor] CGColor]];
	[baseLayer setShadowRadius:5.0];
#endif
}
	


- (void)drawRect:(CGRect)rect 
{
    CGContextRef currentContext = UIGraphicsGetCurrentContext();
	
	CGGradientRef glossGradient;
	CGColorSpaceRef rgbColorspace;
	size_t num_locations = 2;
	CGFloat locations[2] = { 0.0, 1.0 };
	CGFloat components[8] = { 0.904, 0.904, 0.904, 1.000,  // Start color
		0.931, 0.949, 0.949, 1.000 }; // End color
	
	rgbColorspace = CGColorSpaceCreateDeviceRGB();
	glossGradient = CGGradientCreateWithColorComponents(rgbColorspace, components, locations, num_locations);
	
	CGRect currentBounds = self.bounds;
	CGPoint topCenter = CGPointMake(CGRectGetMidX(currentBounds), 0.0f);
	CGPoint midCenter = CGPointMake(CGRectGetMidX(currentBounds), CGRectGetMaxY(currentBounds));
	CGContextDrawLinearGradient(currentContext, glossGradient, topCenter, midCenter, 0);
	
	CGGradientRelease(glossGradient);
	CGColorSpaceRelease(rgbColorspace);
	return;
	CGFloat left = CGRectGetMinX(rect);
	CGFloat top = CGRectGetMinY(rect);
	CGFloat right = CGRectGetMaxX(rect);
	//CGFloat bottom = CGRectGetMaxY(rect);
	CGContextMoveToPoint(currentContext, left, top);
	CGContextSetLineWidth(currentContext, 4.0);
	CGContextSetStrokeColorWithColor(currentContext, [[UIColor darkGrayColor] CGColor]);
	CGContextAddLineToPoint(currentContext, right, top);
	CGContextStrokePath(currentContext);
	CGContextMoveToPoint(currentContext, left, top + 3.0);
	CGContextSetLineWidth(currentContext, 1.0);
	CGContextSetStrokeColorWithColor(currentContext, [[UIColor whiteColor] CGColor]);
	CGContextAddLineToPoint(currentContext, right, top + 3.0);
	CGContextStrokePath(currentContext);
}


@end
