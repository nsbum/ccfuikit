/**
 *   @file CCFStatusPanelView.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2010-07-20 09:16:06
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFStatusPanelView.h"
#import <QuartzCore/QuartzCore.h>

@implementation CCFStatusPanelView


- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        // Initialization code
    }
    return self;
}

- (void)awakeFromNib;
{
	[super awakeFromNib];
	[[self layer] setShadowColor:[[UIColor blackColor] CGColor]];
	[[self layer] setShadowRadius:7.0];
	[[self layer] setShadowOpacity:0.8];
}

/*
// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    // Drawing code
}
*/



@end
