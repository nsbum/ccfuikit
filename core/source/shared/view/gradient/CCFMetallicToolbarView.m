/**
 *   @file CCFMetallicToolbarView.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2010-10-09 09:15:12
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFMetallicToolbarView.h"
#import <QuartzCore/QuartzCore.h>

// -------------------------------------------------------------------------------
//
//	Private method declarations
//
// -------------------------------------------------------------------------------
@interface CCFMetallicToolbarView ()
- (void)_commonGradientInitialization;
@end


@implementation CCFMetallicToolbarView

+ (Class)layerClass;
{
	return [CAGradientLayer class];
}

- (id)initWithFrame:(CGRect)frame;
{
    if( (self = [super initWithFrame:frame]) )
	{
        [self _commonGradientInitialization];
    }
    return self;
}

- (void)awakeFromNib;
{
	[self _commonGradientInitialization];
}

- (void)_commonGradientInitialization;
{
	CAGradientLayer *gradientLayer = (CAGradientLayer *)[self layer];
    UIColor *topColor = [UIColor colorWithWhite:0.950 alpha:1.000];
    UIColor *bottomColor = [UIColor colorWithWhite:0.780 alpha:1.000];
    UIColor *endColor = [UIColor colorWithWhite:0.600 alpha:1.000];
	[gradientLayer setColors:[NSArray arrayWithObjects:(__bridge id)topColor.CGColor,(__bridge id)bottomColor.CGColor,(__bridge id)endColor.CGColor,nil]];
	[gradientLayer setLocations:[NSArray arrayWithObjects:[NSNumber numberWithFloat:0.0],[NSNumber numberWithFloat:0.94],[NSNumber numberWithFloat:1.0],nil]];
}

@end
