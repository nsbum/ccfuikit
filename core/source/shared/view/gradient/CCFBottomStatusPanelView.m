/**
 *   @file CCFBottomStatusPanelView.m
 *   @author Alan Duncan (www.cocoafactory.com)
 *
 *   @date 2011-01-17 09:13:36
 *   @version 1.0
 *
 *   @note Copyright 2011 Cocoa Factory, LLC.  All rights reserved
 */

#import "CCFBottomStatusPanelView.h"
#import <QuartzCore/QuartzCore.h>

@implementation CCFBottomStatusPanelView

- (id)initWithCoder:(NSCoder *)aDecoder;
{
	self = [super initWithCoder:aDecoder];
	if( !self ) return nil;
	
	self.layer.shadowColor = [[UIColor blackColor] CGColor];
	self.layer.shadowRadius = 3.0;
	self.layer.shadowOpacity = 0.6;
	self.layer.shouldRasterize = YES;
	
	return self;
}

@end
